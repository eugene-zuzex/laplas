package common

import (
	"encoding/json"
	"os"
	"path/filepath"
)

type SystemConfiguration struct {
	Token        string
	Username     string
	Password     string
	ItemsPerPage uint
	SSLCert      string
	SSLKey       string
	MySql        string
	WWW          string
	PORT         string
}

var SystemConfig SystemConfiguration

func ReadConfig() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		FailOnError(err, "Failed to read config")
	}
	file, err := os.Open(dir + "/config.json")
	if err != nil {
		FailOnError(err, "Failed to read config")
	}

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&SystemConfig)
	if err != nil {
		FailOnError(err, "Failed to read config")
	}
}
