package common

import (
	"fmt"
	"log"
	"os"

	_ "github.com/jinzhu/gorm"
)

const DEFAULT_PORT = "8080"
const API_PREFIX = "/api"
const ADMIN_PREFIX = "/"
const AUTH_TOKEN_NAME = "auth_token"
const DB_FORMAT = "2006-01-02 15:04:05"

type Data struct {
	Id        uint   `json:"-"`
	Time      int32  `json:"time"`
	IpAddress string `json:"ip"`
	Mac       string `json:"mac"`
	ProgramId string `json:"program_id"`
}

func (Data) TableName() string {
	return "data"
}

var version string
var systemValues map[string]string

func Start() {
	systemValues = make(map[string]string)
	args := os.Args[1:]
	l := len(args)

	for i := 0; i < l; i++ {
		key := args[i][2:]
		if i+1 == l {
			systemValues[key] = "true"
			break
		}
		value := args[i+1]
		if value[0:2] == "--" {
			//some flag was set
			systemValues[key] = "true"
		} else {
			systemValues[key] = value
			i++
		}
	}

	if _, e := systemValues["port"]; !e {
		fmt.Println(systemValues)
		systemValues["port"] = DEFAULT_PORT
	}
}

func SetVersion(v string) {
	version = v
}

func GetVersion() string {
	return version
}

func GetSystemValue(key string) string {
	value, isset := systemValues[key]
	if !isset {
		return ""
	}
	return value
}

func FailOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
		panic(fmt.Sprintf("%s: %s", msg, err))
	}
}
