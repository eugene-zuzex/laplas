package server

import (
	//"fmt"
	"html/template"
	"laplas/common"
	"laplas/database"
	"math"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type tData struct {
	Count       int32
	Data        []common.Data
	TotalPages  int
	CurrentPage int
	Pages       []int8
}

/* View method */
func viewHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	page := vars["page"]
	if len(page) == 0 {
		page = "1"
	}
	p64, _ := strconv.ParseInt(page, 10, 64)
	limit := common.SystemConfig.ItemsPerPage
	offset := uint(p64) - 1
	offset *= limit

	d := &tData{}
	d.CurrentPage = int(p64)
	database.Connection.Model(&common.Data{}).Limit(limit).Offset(offset).Find(&d.Data)

	database.Connection.Model(&common.Data{}).Count(&d.Count)
	f := float64(d.Count) / float64(limit)

	d.TotalPages = int(math.Ceil(f))

	if d.TotalPages > 1 {
		d.Pages = make([]int8, d.TotalPages)
		d.Pages[d.CurrentPage-1] = 1
	}
	loc, _ := time.LoadLocation("Europe/Moscow")
	funcMap := template.FuncMap{
		"format": func(t int32) string {
			date := time.Unix(int64(t), 0)
			return date.In(loc).Format("02.01.2006 15:04:05-07:00")
		},
		"inc": func(i int) int {
			return i + 1
		},
		"dec": func(i int) int {
			if i <= 1 {
				return 1
			}
			return i - 1
		},
	}

	t, _ := template.New("layout").Funcs(funcMap).ParseFiles(templatePath + "index.html")

	t.ExecuteTemplate(w, "layout", d)
}

/* Special function-wrapper */
func use(h http.HandlerFunc, middleware ...func(http.HandlerFunc) http.HandlerFunc) http.HandlerFunc {
	for _, m := range middleware {
		h = m(h)
	}

	return h
}

/* Check Basic HTTP Auth */
func basicAuth(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {

		w.Header().Set("WWW-Authenticate", `Basic realm="Restricted"`)

		username, password, authOK := r.BasicAuth()
		if authOK == false {
			http.Error(w, "Not authorized", 401)
			return
		}

		if username != common.SystemConfig.Username || password != common.SystemConfig.Password {
			http.Error(w, "Not authorized", 401)
			return
		}

		h.ServeHTTP(w, r)
	}
}
