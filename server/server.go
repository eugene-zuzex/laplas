package server

import (
	//"context"
	"fmt"
	"laplas/common"
	"net/http"

	"github.com/gorilla/mux"
)

type Response struct {
	Success bool        `json:"success"`
	Body    interface{} `json:"body"`
}
type Auth struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

var templatePath string

func Start() {
	//templatePath = common.GetSystemValue("template-path")
	templatePath = common.SystemConfig.WWW
	if templatePath == "" {
		common.FailOnError(nil, "Template path is not set")
	}
	//port := common.GetSystemValue("port")
	port := common.SystemConfig.PORT

	r := mux.NewRouter()
	//handlers for API

	r.HandleFunc("/rest", restHandler).Methods("POST")

	//static files
	r.PathPrefix("/assets").Handler(http.StripPrefix("/assets", http.FileServer(http.Dir(templatePath+"assets"))))

	//r.HandleFunc("/logout", logoutHandler).Methods("GET")
	r.HandleFunc("/", use(viewHandler, basicAuth)).Methods("GET")
	r.HandleFunc("/page/{page:[0-9]+}", use(viewHandler, basicAuth)).Methods("GET")

	http.Handle("/", r)

	fmt.Println("HTTP server starting on port " + port + "...")
	var err error
	if len(common.SystemConfig.SSLCert) != 0 && len(common.SystemConfig.SSLKey) != 0 {
		err = http.ListenAndServeTLS(":"+port, common.SystemConfig.SSLCert, common.SystemConfig.SSLKey, nil)
	} else {
		err = http.ListenAndServe(":"+port, nil)
	}

	if err != nil {
		common.FailOnError(err, "Could not start http server")
	}
}
