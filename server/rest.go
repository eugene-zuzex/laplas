package server

import (
	"encoding/json"
	//"fmt"
	"laplas/common"
	"laplas/database"
	"net/http"
)

type restResponse struct {
	Token string `json:"token"`
}

/* API method for data */
func restHandler(w http.ResponseWriter, r *http.Request) {
	if c := checkToken(r); !c {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	d := &common.Data{}
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(&d)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if d.Time == 0 || len(d.IpAddress) == 0 || len(d.Mac) == 0 || len(d.ProgramId) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	database.Connection.Create(d)
	w.WriteHeader(http.StatusOK)
}

/* Check auth via token */
func checkToken(r *http.Request) bool {
	bearer := r.Header.Get("Authorization")
	return bearer == common.SystemConfig.Token
}
