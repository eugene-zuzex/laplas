package database

import (
	"fmt"
	"laplas/common"
	"os"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

var Connection *gorm.DB
var err error

func Start() {
	fmt.Println("Connecting to database...")
	//if url := common.GetSystemValue("database-connection"); url != "" {
	if url := common.SystemConfig.MySql; url != "" {
		Connection, err = gorm.Open("mysql", url)
		if err != nil {
			common.FailOnError(err, "Failed to connect to database")
			return
		}

		fmt.Println("Connected to database!")
	} else {
		fmt.Println("Cannot connect to MySQL server")
		os.Exit(2)
		return
	}

	migrate()
}

func migrate() {

	if !Connection.HasTable(&common.Data{}) {
		Connection.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&common.Data{})
	}

	Connection.AutoMigrate(&common.Data{})

}
