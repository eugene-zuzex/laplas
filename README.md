Для развертывания проекта на сервере:

1. Склонировать проект в папку $GOPATH/src/laplas (название папки обязательно должно совпадать)
2. Скопировать файл $GOPATH/src/laplas/config.json.example в $GOPATH/src/laplas/config.json. Изменить файл $GOPATH/src/laplas/config.json в соответствие с настройками сервера (БД, порт и т.д)
3. Выполнить `go get` в папке $GOPATH/src/laplas
4. Выполнить `go build -i` в папке $GOPATH/src/laplas
Для Linux
5. Скопировать файл laplasd в папку /etc/init.d/. Изменить параметр LAPLAS_PATH на настоящий
6. Выполнить команды `chmod +x laplasd`, `update-rc.d laplasd defaults`
7. Запустить сервис `service laplasd start`