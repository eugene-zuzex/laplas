package main

import (
	"flag"
	"fmt"
	"laplas/common"
	"laplas/database"
	"laplas/server"
	"log"

	"github.com/kardianos/service"
)

type program struct{}

//examle : Elog.Info(1, "starting")
//var Elog eventlog.Log
var version string

func (p *program) Start(s service.Service) error {
	// Start should not block. Do the actual work async.
	go p.run()
	return nil
}

func (p *program) run() {
	common.ReadConfig()
	common.SetVersion(version)
	database.Start()
	server.Start()
	fmt.Println("VSort started!")
	//Elog.Info(1, "VSort started!")
}

func (p *program) Stop(s service.Service) error {
	// Stop should not block. Return with a few seconds.
	return nil
}

func main() {
	//Elog = *elog1

	svcFlag := flag.String("service", "", "Control the system service.")
	flag.Parse()

	svcConfig := &service.Config{
		Name:        "laplas.server",
		DisplayName: "Laplas Daemon",
		Description: "Laplas Daemon Web Server",
	}

	prg := &program{}
	s, err := service.New(prg, svcConfig)
	if err != nil {
		log.Fatal(err)
	}

	if len(*svcFlag) != 0 {
		err := service.Control(s, *svcFlag)
		if err != nil {
			log.Printf("Valid actions: %q\n", service.ControlAction)
			log.Fatal(err)
		}
		return
	}

	err = s.Run()
	if err != nil {
		log.Fatal(err)
	}
}
